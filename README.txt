Readme
------
This module displays paged comments; that is, it displays all comments and a selected user's comments.

A working example can be found at http://www.pedigreemusic.com/comments

Send feedback to Martin Malloy at: http://drupal.org/user/190729/contact


Requirements
------------
This module requires Drupal 5.x.


Installation
------------
1. Copy the comments_page directory to the Drupal modules directory - preferably sites/all/modules.

2. Enable "Comments Page" in the modules administration section.

3. That's it! Access the comments by visiting http://drupalsite.com/comments
	or http://drupalsite.com/comments/xxx - where drupalsite.com is your website and xxx is the user ID.

Credits
-------
Written by Martin Malloy.